using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlColsion : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        ControlCamara controlCamara = collision.gameObject.GetComponent<ControlCamara>();

        if (controlCamara != null)
        {
            controlCamara.PermitirMovimientoMouse(false);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        ControlCamara controlCamara = collision.gameObject.GetComponent<ControlCamara>();

        if (controlCamara != null)
        {
            controlCamara.PermitirMovimientoMouse(true);
        }
    }
}
