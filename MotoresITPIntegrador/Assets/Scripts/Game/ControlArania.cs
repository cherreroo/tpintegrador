using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlArania : MonoBehaviour
{
    public GameObject aranita;

    private int aranitasActuales = 0;

    public void Aranitas()
    {
        DivideFragmentador();
    }

    private void DivideFragmentador()
    {
        for (int i = 0; i < 11; i++)
        {
            Vector3 posicionAranita = transform.position + Random.insideUnitSphere * 2.0f;
            Instantiate(aranita, posicionAranita, Quaternion.identity);
            aranitasActuales++;
        }
    }
}
