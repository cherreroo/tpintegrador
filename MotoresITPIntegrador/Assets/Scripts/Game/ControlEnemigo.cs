using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlEnemigo : MonoBehaviour
{
    public NavMeshAgent agente;
    public Transform objetivo;
    public float rapidez;
    void Start()
    {
        objetivo = GameObject.FindGameObjectWithTag("Jugador").transform;
    }
    void Update()
    {
        agente.speed = rapidez;
        agente.SetDestination(objetivo.position);
    }
}
