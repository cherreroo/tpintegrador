using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Zanquero : MonoBehaviour
{
    public float vida = 100;
    public GameObject titerePrefab;
    public Slider barrahp;
    public float vidaMaxima = 100f;
    public float vidaMinima = 0f;

    private void Start()
    {
        barrahp.minValue = vidaMinima;
        barrahp.maxValue = vidaMaxima;
        barrahp.value = vida;
    }
    private void Update()
    {
        barrahp.value = vida;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Bala"))
        {
            Debug.Log("Estoy colisionando con la bala");
            RestarVida(20);
        }
        else if (other.gameObject.CompareTag("Aranita"))
        {
            Debug.Log("Estoy colisionando con la arania");
            RestarVida(5);
        }
    }
    public void RestarVida(int cantidad)
    {
        vida -= cantidad;
        vida = Mathf.Clamp(vida, vidaMinima, vidaMaxima);
        barrahp.value = vida;

        if (vida <= 0)
        {
            Destroy(this.gameObject);
            Instantiate(titerePrefab, new Vector3(0, 2, 0), Quaternion.identity); 
        }
    }
}
