using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrar : MonoBehaviour
{
    public GameObject ObjetoAgarrar;
    public GameObject ObjetoAgarrado;
    public Transform Manos;
    private GameManager gameManager;
    public Transform defaultPosition;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    void Update()
    {
        if (
            ObjetoAgarrar != null
            && ObjetoAgarrar.GetComponent<Coleccionable>().sePuedeAgarrar == true
            && ObjetoAgarrado == null
        )
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                GestorDeAudio.instancia.ReproducirSonido("Coleccionable");
                ObjetoAgarrado = ObjetoAgarrar;
                ObjetoAgarrado.GetComponent<Coleccionable>().sePuedeAgarrar = false;
                ObjetoAgarrado.transform.SetParent(defaultPosition);
                ObjetoAgarrado.transform.position = defaultPosition.position;
                //ObjetoAgarrado.transform.position = Manos.position;
                ObjetoAgarrado.GetComponent<Rigidbody>().useGravity = false;
                ObjetoAgarrado.GetComponent<Rigidbody>().isKinematic = true;
                if (ObjetoAgarrar.CompareTag("Titere"))
                {
                    gameManager.RecolectarObjeto();
                    Destroy(ObjetoAgarrar);
                }
            }
        }
        else if (ObjetoAgarrado != null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                ObjetoAgarrado.GetComponent<Coleccionable>().sePuedeAgarrar = true;
                ObjetoAgarrado.transform.SetParent(null);
                ObjetoAgarrado.transform.position = defaultPosition.position;
                ObjetoAgarrado.GetComponent<Rigidbody>().useGravity = true;
                ObjetoAgarrado.GetComponent<Rigidbody>().isKinematic = false;
                ObjetoAgarrado = null;
            }
        }
    }
}
