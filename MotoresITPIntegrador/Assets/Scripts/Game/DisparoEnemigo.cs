using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEnemigo : MonoBehaviour
{
    public GameObject torta;
    public Transform tortaPos;
    public float tortaVel;
    private Transform jugadorPos;
    public float rangoDisparo = 10f;
    void Start()
    {
        jugadorPos = GameObject.FindGameObjectWithTag("Jugador").transform;
        StartCoroutine(DispararTortaCorutina());
    }

    IEnumerator DispararTortaCorutina()
    {
        while (true)
        {
            float distanciaAlJugador = Vector3.Distance(transform.position, jugadorPos.position);
            Debug.Log("Distancia al jugador: " + distanciaAlJugador);
            if (distanciaAlJugador < rangoDisparo)
            {
                disparoTorta();
                Debug.Log("Disparo torta");
            }
           // disparoTorta();
            yield return new WaitForSeconds(2f); 
        }
    }
    void disparoTorta()
    {
        Vector3 direccionDelJugador = jugadorPos.position - transform.position;
        GameObject nuevaTorta;
        nuevaTorta = Instantiate(torta, tortaPos.position, tortaPos.rotation);
        Rigidbody rb = nuevaTorta.GetComponent<Rigidbody>();
        rb.GetComponent<Rigidbody>().AddForce(direccionDelJugador * tortaVel * Time.deltaTime,ForceMode.VelocityChange);
    }
}
//Hacer corutina