using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaDelEnemigo : MonoBehaviour
{
    public float vida = 100;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Aranita"))
        {
            RestarVidaEnemigo(20);
        }
    }

    public void RestarVidaEnemigo(float danio)
    {
        vida -= danio;
        Debug.Log("Recibí " + danio + " de daño. Vida restante: " + vida);
        if (vida <= 0)
        {
            Destroy(this.gameObject);
            Debug.Log("Mataste al enemigo");
        }
    }
}
