using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Persecucion : MonoBehaviour
{
    private Transform jugador;
    public int rapidez;

    void Start()
    {
        jugador = GameObject.Find("CuerpoJugador").transform;
    }

    private void Update()
    {
        if (jugador != null)
        {
            Vector3 direccion = (jugador.position - transform.position).normalized;

            transform.Translate(direccion * rapidez * Time.deltaTime);
        }
    }
}
