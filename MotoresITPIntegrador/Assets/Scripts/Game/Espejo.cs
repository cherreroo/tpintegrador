using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Espejo : MonoBehaviour
{
    public float radioBusqueda = 5f;
    public Image imagenHabilidadDisponible;
    public Image imagenRecargando;
    private float tiempoRecarga = 1f; 
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if(PuedeUsarHabilidad())
            {
                ReflejarTorta();
                StartCoroutine(RecargaHabilidad(5f));
            }
        }
    
    if (PuedeUsarHabilidad())
    {
        ActualizarImagen(imagenHabilidadDisponible);
    }
    else
    {
        ActualizarImagen(imagenRecargando);
    }
    }

    void ReflejarTorta()
    {
        GameObject torta = ObtenerTortaCercana();

        if (torta != null)
        {
            Rigidbody tortaRigidbody = torta.GetComponent<Rigidbody>();

            if (tortaRigidbody != null)
            {
                tortaRigidbody.velocity = -tortaRigidbody.velocity;
                GameObject payasoGameObject = GameObject.FindWithTag("Payaso");

                if (payasoGameObject != null)
                {
                    RecibirDanioDos recibirDanio = payasoGameObject.GetComponent<RecibirDanioDos>();

                    if (recibirDanio != null)
                    {
                        recibirDanio.MarcarTortaDelJugador();
                    }
                    else
                    {
                        Debug.Log("El GameObject con la etiqueta 'Payaso' no tiene el script RecibirDanioDos.");
                    }
                }
                else
                {
                    Debug.Log("No se encontró un GameObject con la etiqueta 'Payaso'.");
                }
            }
        }
    }
    GameObject ObtenerTortaCercana()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioBusqueda);

        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Torta"))
            {
                return collider.gameObject;
            }
        }
        return null;
    }
    private bool PuedeUsarHabilidad()
    {
        return Time.time >= tiempoRecarga;
    }

    private IEnumerator RecargaHabilidad(float tiempoRecarga)
    {
        yield return new WaitForSeconds(tiempoRecarga);
        this.tiempoRecarga = Time.time + tiempoRecarga;
    }
    
    void ActualizarImagen(Image nuevaImagen)
    {
        if (imagenHabilidadDisponible != null && imagenRecargando != null)
        {
        imagenHabilidadDisponible.enabled = (nuevaImagen == imagenHabilidadDisponible);
        imagenRecargando.enabled = (nuevaImagen == imagenRecargando);
        }
        else
        {
            Debug.LogError("El GameObject no tiene un componente Image.");
        }
    }

}
