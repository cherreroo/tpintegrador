using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoPayaso : MonoBehaviour
{
    public GameObject tortaPrefab;
    public Transform puntoDisparo;
    public float velocidadTorta = 5f;
    public float tiempoDisparo = 2f;
    public float rangoDisparo = 10f;

    private Transform jugadorPos;

    void Start()
    {
        jugadorPos = GameObject.FindGameObjectWithTag("Jugador").transform;
        StartCoroutine(DispararTortasPeriodicamente());
    }

    IEnumerator DispararTortasPeriodicamente()
    {
        while (true)
        {
            float distanciaAlJugador = Vector3.Distance(transform.position, jugadorPos.position);
            Debug.Log("Distancia al jugador: " + distanciaAlJugador);

            if (distanciaAlJugador < rangoDisparo)
            {
                DispararTorta();
                Debug.Log("Disparo torta");
            }

            yield return new WaitForSeconds(tiempoDisparo);
        }
    }

    void DispararTorta()
    {
        Vector3 direccionDelJugador = jugadorPos.position - transform.position;
        GameObject nuevaTorta = Instantiate(tortaPrefab, puntoDisparo.position, puntoDisparo.rotation);
        Rigidbody rb = nuevaTorta.GetComponent<Rigidbody>();
        rb.velocity = direccionDelJugador.normalized * velocidadTorta;
    }
}
