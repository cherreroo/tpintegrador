using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Payaso : MonoBehaviour
{
    public GameObject tortaPrefab;
    public Transform puntoDisparo;
    public float velocidadTorta = 5f;
    public float tiempoDisparo = 2f;

    void Start()
    {
        StartCoroutine(DispararTortas());
    }

    IEnumerator DispararTortas()
    {
        while (true)
        {
            yield return new WaitForSeconds(tiempoDisparo);
            DispararTorta();
        }
    }

    void DispararTorta()
    {
        GameObject torta = Instantiate(tortaPrefab, puntoDisparo.position, puntoDisparo.rotation);
        Rigidbody rb = torta.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.velocity = puntoDisparo.forward * velocidadTorta;
        }
    }
}
