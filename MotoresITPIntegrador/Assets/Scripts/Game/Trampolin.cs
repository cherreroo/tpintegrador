using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampolin : MonoBehaviour
{
    [Range(100,10000)]
    public float alturaRebote;

    void OnCollisionEnter(Collision collision)
    {
        GameObject trampolin = collision.gameObject;
        Rigidbody rb = trampolin.GetComponent<Rigidbody>();
        rb.AddForce(Vector3.up * alturaRebote);
    }
}
