using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecibirDanioDos : MonoBehaviour
{
    public int vida = 100;
    public GameObject titerePrefab;
    private bool tortaDelJugador = false;
    public Slider barrahp;
    public float vidaMaxima = 100f;
    public float vidaMinima = 0f;

    private void Start()
    {
        barrahp.minValue = vidaMinima;
        barrahp.maxValue = vidaMaxima;
        barrahp.value = vida;
    }

    private void Update()
    {
        barrahp.value = vida;
    }
   // public ControlAranitas enemigo2;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Aranita"))
        {
            Debug.Log("Estoy colisionando con la arania");
            RestarVida(5);
        }
        if (other.gameObject.CompareTag("Torta"))
        {
            if (tortaDelJugador)
            {
                Debug.Log("Estoy colisionando con la Torta");
                RestarVida(100);
                tortaDelJugador = false; 
            }
        }
        if (other.gameObject.CompareTag("Bala"))
        {
            if (tortaDelJugador)
            {
                Debug.Log("Estoy colisionando con la Bala");
                RestarVida(20);
            }
        }
    }
    public void RestarVida(int cantidad)
    {
        vida -= cantidad;

        if (vida <= 0)
        {
            //Destroy(enemigo2.gameObject);
            Destroy(this.gameObject);
            Instantiate(titerePrefab, new Vector3(0, 2, 0), Quaternion.identity); 
        }
    }
    public void MarcarTortaDelJugador()
    {
        tortaDelJugador = true;
    }
}
