using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoAros : MonoBehaviour
{
    bool tengoQueIrArriba = false;
    public int rapidez;

    void Start() { }

    void Update()
    {
        if (tengoQueIrArriba)
        {
            Arriba();
            if (transform.position.y >= 5)
            {
                tengoQueIrArriba = false;
            }
        }
        else
        {
            Abajo();
            if (transform.position.y <= 2)
            {
                tengoQueIrArriba = true;
            }
        }
    }

    void Arriba()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Abajo()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
