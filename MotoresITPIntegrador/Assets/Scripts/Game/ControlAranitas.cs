using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
public class ControlAranitas : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agente;
    public Transform objetivo;
    public float rapidez;
    public int vida = 50;

    void Start()
    {
        GameObject zanqueroObjeto = GameObject.FindGameObjectWithTag("Zanquero");

        if (zanqueroObjeto != null)
        {
            objetivo = zanqueroObjeto.transform;
        }
        else
        {
            Debug.Log("No se encontró el objeto con el tag 'Zanquero'.");
            objetivo = GameObject.FindGameObjectWithTag("Jugador").transform;

        }
        //objetivo = GameObject.FindGameObjectWithTag("Zanquero").transform;
    }

    void Update()
    {
      if (objetivo != null)
        {
            Vector3 direccion = (objetivo.position - transform.position).normalized;
            transform.Translate(direccion * rapidez * Time.deltaTime);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Bala"))
        {
            RestarVida(10);
        }
    }
    public void RestarVida(int cantidad)
    {
        vida -= cantidad;

        if (vida <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
