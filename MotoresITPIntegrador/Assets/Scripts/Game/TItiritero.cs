using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TItiritero : MonoBehaviour
{
    public int marionetasActivas = 3;
    public int vida = 100;

    public void MarionetaEliminada()
    {
        marionetasActivas--;

        if (marionetasActivas == 2)
        {
            RecibirDanio(50);
        }
        else if (marionetasActivas == 1)
        {
            RecibirDanio(40);
        }
        else if (marionetasActivas == 0)
        {
            RecibirDanio(50);
        }
    }
    private void RecibirDanio(int cantidad)
    {
        vida -= cantidad;

        if (vida <= 0)
        {
            Destroy(gameObject);
        }
    }

}
