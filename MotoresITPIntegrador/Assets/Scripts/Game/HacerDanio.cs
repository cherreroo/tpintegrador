using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HacerDanio : MonoBehaviour
{
    public float cantidadDanio;
  //  public Ataqueadistancia enemigo;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            other.GetComponent<VidaDelJugador>().RestarVida(cantidadDanio);
        }
        else if (other.CompareTag("Escudo"))
        {
            gameObject.GetComponent<SphereCollider>().enabled = false;
            Debug.Log("Escudo activado");
        }
    }
   /* public void DanioExplosion()
    {
        enemigo.Explosion();
    }*/
}
