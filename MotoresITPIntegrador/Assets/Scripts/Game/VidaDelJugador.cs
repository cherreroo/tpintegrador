using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VidaDelJugador : MonoBehaviour
{
    public float vida = 100;
    public float vidaMax = 100;
    public Image Barra;

    void Update()
    {
        Actualizacion();
    }
    void Actualizacion()
    {
        Barra.fillAmount = vida / vidaMax;
    }   
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Lava"))
        {
            Debug.Log("Me estoy quemando");
            RestarVida(50);
        }
        if (other.gameObject.CompareTag("Caramelo"))
        {
            Debug.Log("Me comi un caramelo");
            SumarVida(50);
        }
    }
    public void RestarVida(float danio)
    {
        vida -= danio;

        if (vida <= 0)
        {
            Debug.Log("Estas muerto");
            Cursor.lockState = CursorLockMode.None; 
            Cursor.visible = true; 
            SceneManager.LoadScene("GAMEOVER");
        }
    }
    public void SumarVida(float curacion)
    {
        vida += curacion;
        vida = Mathf.Clamp(vida, 0, vidaMax);
    }
}
