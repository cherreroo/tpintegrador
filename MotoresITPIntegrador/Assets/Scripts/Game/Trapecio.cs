using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trapecio : MonoBehaviour
{
    private ControlJugador pm;
    public Transform cam;
    public Transform casquillo;
    public LayerMask queSeAgarra;
    public LineRenderer lr;
    public float distanciaMaxima;
    public float tiempo;
    public float sobrepaso;
    private Vector3 puntoDeAgarre;

    public float coolDown;
    private float coolDownTimer;

    public KeyCode trapecioKey = KeyCode.Alpha1;

    private bool colgando;
    public Agarrar scriptAgarrar;
    private void Start()
    {
        pm = GetComponent<ControlJugador>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(trapecioKey) && scriptAgarrar.ObjetoAgarrado != null)
        {
            EmpezarAgarre();
        }
        if(coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }
    }

    private void LateUpdate()
    {
        if(colgando)
        {
            lr.SetPosition(0, casquillo.position);
        }
    }

    private void EmpezarAgarre()
    {
        if (coolDownTimer > 0) 
        {
            return;
        }
        colgando = true;
        pm.freeze = true;
        RaycastHit hit;
        if(Physics.Raycast(cam.position, cam.forward, out hit, distanciaMaxima, queSeAgarra))
        {
            puntoDeAgarre = hit.point;

            Invoke(nameof(HacerAgarre),tiempo);
        }
        else
        {
            puntoDeAgarre = cam.position + cam.forward * distanciaMaxima;
            Invoke(nameof(TerminarAgarre),tiempo);
        }
        lr.enabled = true;
        lr.SetPosition(1, puntoDeAgarre);
    } 

    private void HacerAgarre()
    {
        pm.freeze = false;  
        
        Vector3 lowestPoint = new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z);

        float grapplePointRelativeYPos = puntoDeAgarre.y - lowestPoint.y;
        float highestPointOnArc = grapplePointRelativeYPos + sobrepaso;

        if (grapplePointRelativeYPos < 0) highestPointOnArc = sobrepaso;

        pm.Colgarse(puntoDeAgarre, highestPointOnArc);

        Invoke(nameof(TerminarAgarre), 1f);     
    } 
    private void TerminarAgarre()
    {
        pm.freeze = false;
        colgando = false;
        coolDownTimer = coolDown;
        lr.enabled = false;
    } 

}
