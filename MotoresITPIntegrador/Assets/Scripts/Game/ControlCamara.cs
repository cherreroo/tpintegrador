using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public static ControlCamara Instance { get; private set; }
    Vector2 mouseMirar;
    Vector2 suavidadV;

    public float sensibilidad = 1f;
    public float suavizado = 2.0f;
    public float tiempoEspera = 0.05f;

    GameObject Jugador;
    float tiempoUltimaActualizacion;
    
    bool permitirMovimientoMouse = true;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        Jugador = this.transform.parent.gameObject;
        tiempoUltimaActualizacion = Time.time;
    }

    void Update()
    {
        if (Time.time - tiempoUltimaActualizacion < tiempoEspera)
        {
            return; 
        }

        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md.x = Mathf.Clamp(md.x,-20f ,20f ); 
        md.y = Mathf.Clamp(md.y, -5f, 5f); 

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f/suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f/suavizado);

       if (permitirMovimientoMouse)
        {
            mouseMirar += suavidadV;
            mouseMirar.y = Mathf.Clamp(mouseMirar.y, -80f, 80f);
            transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
            Jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, Jugador.transform.up);
        }

        tiempoUltimaActualizacion = Time.time;
    }
    public void PermitirMovimientoMouse(bool permitir)
    {
        permitirMovimientoMouse = permitir;
    }
}
