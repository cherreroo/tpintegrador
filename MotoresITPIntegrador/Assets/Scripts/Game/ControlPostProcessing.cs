using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ControlPostProcessing : MonoBehaviour
{
    public Volume volumenGlobal;
    public float tiempoDeEfecto = 5f;
   // private bool activarEfectos = false;

    void Start()
    {
        volumenGlobal.enabled = false; 
    }
    public void OnTriggerEnter(Collider other) 
    {
        if (other.CompareTag("Torta")) 
        {
            Debug.Log("Te comiste un tortazo");
            AplicarEfecto();
        }
    }
    private void AplicarEfecto()
    {
        volumenGlobal.enabled = true;
        StartCoroutine(DesactivarEfectoDespuesDeTiempo());
    }

    private IEnumerator DesactivarEfectoDespuesDeTiempo()
    {
        Debug.Log("Iniciando corutina de desactivación de efecto");
        yield return new WaitForSeconds(tiempoDeEfecto);
        volumenGlobal.enabled = false;
        Debug.Log("Efecto desactivado");
    }
}
