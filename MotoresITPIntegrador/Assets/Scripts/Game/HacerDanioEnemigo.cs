using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HacerDanioEnemigo : MonoBehaviour
{
    public int cantidadDanio;

    private void OnTriggerEnter(Collider other)
{
    if (other.CompareTag("Aranita"))
    {
        VidaDelEnemigo vidaEnemigo = other.GetComponent<VidaDelEnemigo>();
        if (vidaEnemigo != null)
        {
            vidaEnemigo.RestarVidaEnemigo(cantidadDanio);
        }
    }
    else if (other.CompareTag("Payaso"))
    {
        RecibirDanioDos recibirDanio = other.GetComponent<RecibirDanioDos>();
        if (recibirDanio != null)
        {
            recibirDanio.RestarVida(cantidadDanio);
        }
    }
    else if (other.CompareTag("Jugador"))
    {
        VidaDelJugador vidaJugador = other.GetComponent<VidaDelJugador>();
        if (vidaJugador != null)
        {
            vidaJugador.RestarVida(cantidadDanio);
        }
    }
    else if (other.CompareTag("Escudo"))
    {
        gameObject.GetComponent<SphereCollider>().enabled = false;
        Debug.Log("Escudo activado");
    }
}
   /* private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Aranita"))
        {
            other.GetComponent<VidaDelEnemigo>().RestarVidaEnemigo(cantidadDanio);
        }
        if (other.CompareTag("Payaso"))
        {
            other.GetComponent<RecibirDanioDos>().RestarVida(cantidadDanio);
        }
        if (other.CompareTag("Jugador"))
        {
            other.GetComponent<VidaDelJugador>().RestarVida(cantidadDanio);
        }
    }*/
}
