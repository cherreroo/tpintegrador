using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuPausa : MonoBehaviour
{
    public GameObject PanelPausa;
    private int numeroEscena = 0;
    // Update is called once per frame
    void Update()
    {
        
    }
    public void Pausa()
    {
        PanelPausa.SetActive(true);
        Time.timeScale = 0;
    }

    public void Reanudar()
    {
        PanelPausa.SetActive(false);
        Time.timeScale = 1;
    }
    public void Salir()
    {
        Application.Quit();
    }
    public void Menu()
    {
        SceneManager.LoadScene(numeroEscena);
    }
}
