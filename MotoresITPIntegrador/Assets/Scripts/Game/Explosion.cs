using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public GameObject explosion;
    public Transform ataquePos;
    public Transform jugador; 
    public float rangoExplosion = 10f;
    private bool explosionHecha = false;
    public float tiempoReinicio = 5f;
    void Update ()
    {
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.position);
        if (distanciaAlJugador < rangoExplosion && !explosionHecha)
        {
           // Explotar(20);
           MostrarExplosion();
           explosionHecha = true;
           StartCoroutine(ReiniciarExplosion());
        }
    }
    IEnumerator ReiniciarExplosion()
    {
        yield return new WaitForSeconds(tiempoReinicio);
        explosionHecha = false;
    }
   /* public void Explotar(int segundos)
    {
        InvokeRepeating("MostrarExplosion", 2, segundos);
    }*/
    public void MostrarExplosion()
    {
        GameObject particulas = Instantiate(explosion, ataquePos.position, ataquePos.rotation);
        Destroy(particulas, 1);
    }
}
