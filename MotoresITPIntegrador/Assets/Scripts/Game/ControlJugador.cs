using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public float dash = 20.0f;
    public Camera camaraPrimeraPersona;
    private Rigidbody rb;
    public LayerMask Piso;
    public float magnitudSalto;
    private int cantidadSaltos;
    public bool EstaEnPiso;
    public GameObject Casquillo;
    public GameObject BalaPreFab;
    public float VelBala;
    public Agarrar scriptAgarrar;
    public GameObject escudo;
    public bool freeze = false;
    public bool agarreActivo;
    public float alturaDeCaida = -10f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        cantidadSaltos = 0;
        GestorDeAudio.instancia.ReproducirSonido("musica");
    }

    void Update()
    {
        float movimientoAdelanteAtras = 0;
        float movimientoCostados = 0;
        /*float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;*/
        
        if (Input.GetKey(KeyCode.LeftShift)) 
        {            
            movimientoAdelanteAtras = Input.GetAxis("Vertical") * dash;
            movimientoCostados = Input.GetAxis("Horizontal") * dash;
        }
        else
        {
            movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
            movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        }

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        Vector3 centroCubo = transform.position;
        Vector3 tamañoCubo = new Vector3(1.0f, 1.0f, 1.0f);
        EstaEnPiso = Physics.CheckBox(centroCubo, tamañoCubo, Quaternion.identity, Piso);
        Vector3 newVelocity = rb.velocity;

        if (Input.GetKeyDown(KeyCode.Space) && cantidadSaltos < 2)
        {
            Salto();
            cantidadSaltos++;
        }
        if (EstaEnPiso)
        {
            cantidadSaltos = 0;
        }
        if (
            Input.GetButtonDown("Fire1")
            && scriptAgarrar.ObjetoAgarrado != null
        )
        {
            Ray ray = camaraPrimeraPersona.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 direccionBala = hit.point - Casquillo.transform.position;
                direccionBala.Normalize();

                GameObject TempBala = Instantiate(
                    BalaPreFab,
                    Casquillo.transform.position,
                    Quaternion.LookRotation(direccionBala)
                );
                Rigidbody TempRb = TempBala.GetComponent<Rigidbody>();
                TempRb.AddForce(direccionBala * VelBala, ForceMode.Impulse);
                Destroy(TempBala, 5.0f);
                Debug.Log("Estas disparando");
            }
        }
        if(freeze)
        {
            rb.velocity = Vector3.zero;           
        }
        if (Input.GetKey("r"))
        {
           ResetScene();
        }
         if (transform.position.y < alturaDeCaida)
        {
            ResetScene();
        }
        if(Input.GetButton("Fire2"))
        {
           escudo.SetActive(true);
        }
       else
       {
            escudo.SetActive(false);
       }
    }
    private void Salto()
    {
        rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        Debug.Log("Esta saltando");
    }
    void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Colgarse(Vector3 targetPosition, float alturaTrayectoria)
    {
        agarreActivo = true;
        velocityToSet = CalcularSalto(transform.position, targetPosition, alturaTrayectoria);
        Invoke(nameof(SetVelocity), 0.1f);
    }
    private Vector3 velocityToSet;

    private void SetVelocity()
    {
        rb.velocity = velocityToSet;
    }
    public Vector3 CalcularSalto(Vector3 startPoint, Vector3 endPoint, float alturaTrayectoria)
    {
        float gravedad = Physics.gravity.y;
        float desplazamientoY = endPoint.y - startPoint.y;
        Vector3 desplazamientoXZ = new Vector3(endPoint.x - startPoint.x, 0f, endPoint.z - startPoint.z);
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravedad * alturaTrayectoria);
        Vector3 velocityXZ = desplazamientoXZ / (Mathf.Sqrt(-2 * alturaTrayectoria/gravedad)
            + Mathf.Sqrt(2* (desplazamientoY - alturaTrayectoria)/ gravedad));
        return velocityXZ + velocityY;    
    }
}
