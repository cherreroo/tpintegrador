using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public int objetosRecolectados = 0;
    public GameObject arania;
    public float Tiempoarania;
    private float Nuevoarania;
    void Update()
    {
        if (Nuevoarania <= 0)
        {
            Nuevoarania = Tiempoarania;
            float minX = -20.0f;
            float maxX = 20.0f;
            float minZ = -20.0f;
            float maxZ = 20.0f;

            float randomX = Random.Range(minX, maxX);
            float randomZ = Random.Range(minZ, maxZ);
            GameObject araniaTemp = Instantiate(
                arania,
                new Vector3(randomX, 6, randomZ),
                Quaternion.identity
            );
            Destroy(araniaTemp, 60);
        }
        Nuevoarania -= Time.deltaTime;
        if(objetosRecolectados == 3)
        {
            Cursor.lockState = CursorLockMode.None; 
            Cursor.visible = true; 
            GestorDeAudio.instancia.PausarSonido("musica");
            SceneManager.LoadScene("Ganaste");
        }
    }
    public void RecolectarObjeto()
    {
        objetosRecolectados++;
        Debug.Log("Partes del titere" + objetosRecolectados);
    }
}
// cuando los objetos sean == 2 instanciar al boss final (titiritero)