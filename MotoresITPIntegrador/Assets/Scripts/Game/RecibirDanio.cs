using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecibirDanio : MonoBehaviour
{
    public int vida = 50;
    public ControlArania controlArania;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Jugador"))
        {
            RestarVida(50);
        }
    }
    public void RestarVida(int cantidad)
    {
        vida -= cantidad;

        if (vida <= 0)
        {
            controlArania.Aranitas();
            Destroy(this.gameObject);
        }
    }
}
