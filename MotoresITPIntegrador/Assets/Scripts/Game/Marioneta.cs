using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marioneta : MonoBehaviour
{
    public int salud = 100;
    public TItiritero enemigoPrincipal;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Bala"))
        {
            RestarVida(50);
        }
    }
    public void RestarVida(int cantidad)
    {
        salud -= cantidad;
    }
    private void Update()
    {
        if (salud <= 0)
        {
            enemigoPrincipal.MarionetaEliminada();
            Destroy(gameObject);
        }
    }
}
