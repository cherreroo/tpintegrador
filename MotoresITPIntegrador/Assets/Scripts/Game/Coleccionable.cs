using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coleccionable : MonoBehaviour
{
    public bool sePuedeAgarrar = true;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Manos")
        {
            other.GetComponentInParent<Agarrar>().ObjetoAgarrar = this.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Manos")
        {
            other.GetComponentInParent<Agarrar>().ObjetoAgarrar = null;
        }
    }
}
