using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeteccionTrapecio : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Trapecio"))
        {
            // Colocar el personaje en el trapecio
            // Puedes hacer esto desactivando la gravedad, estableciendo una posición específica, etc.
            // Por ejemplo, asumiré que quieres que el personaje se convierta en hijo del trapecio.
            transform.parent = other.transform;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Trapecio"))
        {
            // Restablecer la relación padre-hijo al salir del trapecio
            transform.parent = null;
        }
    }
}
