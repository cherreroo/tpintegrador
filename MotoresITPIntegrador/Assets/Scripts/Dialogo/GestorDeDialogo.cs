using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GestorDeDialogo : MonoBehaviour
{
    private Queue<string> oraciones;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI mensajeText;
    public static bool estaEnDialogo;
    public GameObject panel;
    public Button botonComenzar;
    public Button botonComenzarDos;
    public string loadScene;
    void Awake()
    {
        oraciones = new Queue<string>();
    }
    void Update()
    {
        if (estaEnDialogo)
        {
            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }
    public void ComenzarDialogo(Dialogo _dialogo)
    {
        estaEnDialogo = true;
        panel.SetActive(true);
        nameText.text = _dialogo.nombre;
        oraciones.Clear();
        foreach (string oracion in _dialogo.oraciones)
        {
            oraciones.Enqueue(oracion);
        }
    }
    public void MostrarSiguienteOracion()
    {
        if (oraciones.Count == 0)
        {
            TerminarDialogo();
            return;
        }
        
        string oracion = oraciones.Dequeue();
        mensajeText.text = oracion;

    }
    private void TerminarDialogo()
    {
        Debug.Log("Termine el dialogo");
        estaEnDialogo = false;
        panel.SetActive(false);
        botonComenzar.gameObject.SetActive(true);
        botonComenzarDos.gameObject.SetActive(true);
    }
    public void ComenzarJuego()
    {
        SceneManager.LoadScene(loadScene);
    }
}
// Ganaste.text = "";