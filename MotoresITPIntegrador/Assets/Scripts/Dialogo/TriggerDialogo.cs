using UnityEngine;

public class TriggerDialogo : MonoBehaviour
{
    public Dialogo dialogo;

    public void DialogoTrigger()
    {
        FindObjectOfType<GestorDeDialogo>().ComenzarDialogo(dialogo);
    }
}
