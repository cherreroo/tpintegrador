using UnityEngine;
using TMPro;

[System.Serializable]
public class Dialogo 
{
    public string nombre;
    [TextArea(3,10)]
    public string[] oraciones;

}
