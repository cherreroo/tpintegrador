using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Terminado : MonoBehaviour
{
    public int numeroEscena;

    public void Reiniciar()
    {
        SceneManager.LoadScene(numeroEscena);
    }

    public void Salir()
    {
        //UnityEditor.Application.isPlaying = false;
        Application.Quit();
    }
}
