using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestorVolumen : MonoBehaviour
{
    public Slider slider;
    public float rangoSlider;


    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("volumenAudio", 0.5f); //valor que permite guardar la posicion donde se guardo el slider.
        AudioListener.volume = slider.value; //el volumen es igual al valor del slider. 
    }

    void Update()
    {
        
    }
    public void ChangeSlider(float valor) // el valor del slider va a ser igual al slider value del inspector
    {
        rangoSlider = valor;
        PlayerPrefs.SetFloat("volumenAudio", rangoSlider); //le ponemos un valor.
        AudioListener.volume = slider.value; 
    }
}
