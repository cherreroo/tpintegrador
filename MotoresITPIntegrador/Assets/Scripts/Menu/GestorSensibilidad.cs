using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestorSensibilidad : MonoBehaviour
{
   public Slider slider;
   public float sensibilidadActual;
   void Start()
    {
        slider.value = PlayerPrefs.GetFloat("sensibilidadMouse", 0.5f);
        sensibilidadActual = slider.value;
    }

    void Update()
    {
        float movimientoMouseX = Input.GetAxis("Mouse X") * sensibilidadActual * Time.deltaTime;
        float movimientoMouseY = Input.GetAxis("Mouse Y") * sensibilidadActual * Time.deltaTime;
    }

    public void CambiarSensibilidad(float valor)
    {
        sensibilidadActual = valor;
        PlayerPrefs.SetFloat("sensibilidadMouse", sensibilidadActual);
    }
}
