using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoMenu : MonoBehaviour
{
    float mousePosX;
    float mousePosY;
    public float movimiento;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mousePosX = Input.mousePosition.x;
        mousePosY = Input.mousePosition.y;

        this.GetComponent<RectTransform>().position = new Vector2(
            (mousePosX / Screen.width) * movimiento + (Screen.width / 2),
            (mousePosY / Screen.height) * movimiento +(Screen.height / 2));
    }
}
